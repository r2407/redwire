$(function () {
    var nouvelleBD = [];
    var idDeLaSeriePourEnregistrement = "";
    $("#titre").blur(
        function () {

            for (i = 1; i < albums.size; i++) {
                if (albums.has(i.toFixed(0))) {

                    if ((albums.get(i.toFixed(0)).titre).toUpperCase() == ($("#titre").val()).toUpperCase()) {
                        alert("Le titre existe déjà : "
                            + "\nTitre : " + albums.get(i.toFixed(0)).titre
                            + "\nNuméros : " + albums.get(i.toFixed(0)).numero);
                        setTimeout(function () { $("#titre").focus() }, 100);
                    }
                }
            };
        })

    $("#numAlbum").blur(function () {
        for (let i = 1; i < series.size; i++) {
            if (series.has(i.toFixed(0))) {
                if (series.get(i.toFixed(0)).nom.toUpperCase() == $("#serie").val().toUpperCase()) {
                    var idDeLaSerie = i.toFixed(0);
                    break;

                }
                else { idDeLaSerie = (parseInt(Array.from(series.keys()).pop()) + 1); }
            }

        }

        var numeroDAlbum = $("#numAlbum").val();
        if (numeroDAlbum.length < 2) {
            numeroDAlbum = "0" + numeroDAlbum;

        }

        for (let i = 1; i < albums.size; i++) {
            if (albums.has(i.toFixed(0))) {

                /*en cas de chiffre unique faire qqch mais demain*/

                if (albums.get(i.toFixed(0)).numero == numeroDAlbum && albums.get(i.toFixed(0)).idSerie == idDeLaSerie) {
                    alert("Le Numero d'album existe déjà : "
                        + "\nTitre : " + albums.get(i.toFixed(0)).titre
                        + "\nSerie : " + series.get(idDeLaSerie).nom);
                    setTimeout(function () { $("#numAlbum").focus() }, 100);
                }
            }
        };

    }

    );



    $("#btnEnregistrer").click(function () {
        for (let i = 1; i < series.size; i++) {
            if (series.has(i.toFixed(0))) {
                if (series.get(i.toFixed(0)).nom.toUpperCase() == $("#serie").val().toUpperCase()) {
                    var idDeLaSerie = i.toFixed(0);
                    break;

                }
                else { idDeLaSerie = (parseInt(Array.from(series.keys()).pop()) + 1); }
            }

        }
        var result = false
        $(".champ").each(function () {

            if ($(this).val() == "") {
                alert("Le champ : " + $(this).attr("name") + " est vide");
                result = false;


            } else { result = true }

            return result;
        });


        if (result == true) {
            for (let i = 1; i < series.size; i++) {
                if (auteurs.has(i.toFixed(0))) {
                    if (auteurs.get(i.toFixed(0)).nom.toUpperCase() == $("#auteur").val().toUpperCase()) {
                        var idDeLAuteur = i.toFixed(0);
                        break;

                    }
                    else { idDeLAuteur = (parseInt(Array.from(auteurs.keys()).pop()) + 1); }
                }
            }

            infoNouvelleBD = {
                'titre': $("#titre").val(), 'numero': $("#numAlbum").val(),
                'idSerie': idDeLaSerie, 'idAuteur': idDeLAuteur, 'prix': $("#prix").val()
            }
            idDeLAlbum = (parseInt(Array.from(albums.keys()).pop()) + 1);
            nouvelleBD = infoNouvelleBD

            albums.set(idDeLAlbum, nouvelleBD);
            // alert(
            //     "l'album a été enregistré"
            //     + "\nTitre : " + albums.get(idDeLAlbum).titre
            //     + "\nSérie : " + albums.get(idDeLAlbum).idSerie
            //     + "\nAuteur : " + albums.get(idDeLAlbum).idAuteur
            //     + "\nNuméros : " + albums.get(idDeLAlbum).numero
            //     + "\nPrix : " + albums.get(idDeLAlbum).prix);
            $("#myBd").text("");
            $("#myBd").append(
                $("<p></p>").text("Titre : " + albums.get(idDeLAlbum).titre),
                $("<p></p>").text("Série : " + albums.get(idDeLAlbum).idSerie),
                $("<p></p>").text("Auteur : " + albums.get(idDeLAlbum).idAuteur),
                $("<p></p>").text("Numéros : " + albums.get(idDeLAlbum).numero),
                $("<p></p>").text("Prix : " + albums.get(idDeLAlbum).prix),
                $("<img>").attr('id', 'imgDeConf'),

            );
            $("#imgDeConf").attr('src', "albums/" + $("#couvAlbum").val().replace(/C:\\fakepath\\/i, '')),
                $("#imgDeConf").attr('width', "200px");
            $("#myModal").toggle();

            $(window).click(function (e) {
                if (e.target == modal) {
                    $("#myModal").hide();
                }
            });
            $(".close").click(function () {
                $("#myModal").hide();
            });

        }
    });

});


























