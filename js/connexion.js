//Les variables :

var btnlog = document.getElementById("log");
var btnenter = document.getElementById("enter");
var btnexit = document.getElementById("exit");
var formLog = document.getElementById("formulaireLog")


//Les RegEx de contrôle :

var controleNom = /([^a-zA-Zâêîôûàèùòÿäëïöüÿéãñõ])/
var controleMotDePasse = /([^a-zA-Z0-9âêîôûàèùòÿäëïöüÿéãñõ!-_])/

//Les abonnements :

formLog.addEventListener("keyup", testbouton)
btnlog.addEventListener("click", controleChamp);
btnenter.addEventListener("click", afficherConnexion);
btnexit.addEventListener("click", deconnecter);

//Fonction au chargement de la page :
window.onload = verificationStatutConnexion;

//Test si touche entrée :
function testbouton(k) {
    if (k.keyCode === 13) {
        controleChamp();
    }
}

//Contrôle des champs du login :
function controleChamp() {
    var barreUserName = document.getElementById("username").value;
    var barrePassword = document.getElementById("password").value;

    if (barreUserName == "" || barrePassword == "") {
        alert("Un des champs est vide");
    }
    else if (controleNom.test(barreUserName) || controleMotDePasse.test(barrePassword)) {
        alert("Caractère invalide présent dans la saisie");
    }
    else {
        testlog();
    }

}

//Test si présence couple ID dans la map
function testlog() {
    var barreUserName = document.getElementById("username").value;
    var barrePassword = document.getElementById("password").value;
    var btest = false;
    for (let i = 1; i <= identifiants.size; i++) {

        var userName = identifiants.get(i).nomUtilisateur;
        var password = identifiants.get(i).motDePasse;
        if (userName == barreUserName && password == barrePassword) {
            btest = true;
            sessionStorage.setItem('Idconnexion', identifiants.get(i).id);
            var divModal = document.getElementById("formulaireLog");
            divModal.style.display = "none";
            btnenter.style.display = "none";
            btnexit.style.display = "block";
            afficheMessage(btest, i);

        }
        else if (userName == barreUserName && password != barrePassword) {
            btest = false;

        }

    }
    if (!btest) {
        var divModal = document.getElementById("formulaireLog");
        divModal.style.display = "none";
        afficheMessage(btest);
    }
}

//Afficher la modale de connexion :
function afficherConnexion() {

    var divModal = document.getElementById("formulaireLog");
    divModal.style.display = "inline";





    window.onclick = function (event) {
        if (event.target == divModal) {
            divModal.style.display = "none";
        }
    }

}

//Afficher un message d'information (erreur ou succès):
function afficheMessage(bool, indice) {
    var divInfos = document.getElementById("messageInfo");
    var dataInfos = document.getElementById("dataInfo");
    var btestadmin = false;
    var bAdmin = false;
    if (bool) {
        if (identifiants.get(indice).compte == "administrateur") {
            bAdmin = true;
            dataInfos.innerHTML = "";
            dataInfos.innerHTML = "Bienvenue votre généralisime " + identifiants.get(indice).nom + " " + identifiants.get(indice).prenom;
            divInfos.style.display = "inline";
        }
        else {
            dataInfos.innerHTML = "";
            if (identifiants.get(indice).compte == "administrateur") {
                btestadmin = true;
                dataInfos.innerHTML = "Bienvenue votre généralissime " + identifiants.get(indice).nom + " " + identifiants.get(indice).prenom;

            }
            else {

                dataInfos.innerHTML = "Bienvenue " + identifiants.get(indice).nom + " " + identifiants.get(indice).prenom;

            }
            divInfos.style.display = "inline";
            dataInfos.innerHTML = "Bienvenue " + identifiants.get(indice).nom + " " + identifiants.get(indice).prenom;
            divInfos.style.display = "inline";
        }

    }

    else {
        dataInfos.innerHTML = "";
        dataInfos.innerHTML = "Erreur dans le nom d'utilisateur ou le mot de passe";
        divInfos.style.display = "inline";

    }
    window.onclick = function (event) {
        if (event.target == divInfos) {
            divInfos.style.display = "none";
            if (!bool) {
                afficherConnexion();
            }
            else if (bAdmin) {
                window.location.replace("ajoutbd.html");
            }
        }
    }

}
//Modifier affichage + session storage lors de la déconnexion :
function deconnecter() {
    var divInfos = document.getElementById("messageInfo");
    var dataInfos = document.getElementById("dataInfo");
    btnenter.style.display = "block";
    btnexit.style.display = "none";
    dataInfos.innerHTML = "";
    dataInfos.innerHTML = "Aurevoir et à bientôt !"
    divInfos.style.display = "inline";


    sessionStorage.setItem("Idconnexion", 0);
    window.onclick = function (event) {
        if (event.target == divInfos) {
            divInfos.style.display = "none";

        }

    }
}

//Vérifier au chargement d'une nouvelle page si déjà connecté :

function verificationStatutConnexion (){
    let valeurStatut = sessionStorage.getItem("Idconnexion");
    if(valeurStatut >0){
        btnenter.style.display = "none";
        btnexit.style.display = "block";
    }
    else{
        btnenter.style.display = "block";
        btnexit.style.display = "none";    }

}

