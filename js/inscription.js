//Les variables :

var monNom = document.getElementById("monNom");
var monPrenom = document.getElementById("monPrenom");
var monUtilisateur = document.getElementById("monUtilisateur");
var monMotdepasse = document.getElementById("monMotdepasse");
var monMail = document.getElementById("monMail");
var monAdresse = document.getElementById("monAdresse");
var maVille = document.getElementById("maVille");
var monCP = document.getElementById("monCP");
var btnRegister = document.getElementById("register");

//Booléens de contrôle des champs :
var bNomOk = false;
var bPrenomOk = false;
var bUtilisateurOk = false;
var bMotdepasseOk = false;
var bMailOk = false;
var bAdresseOk = false;
var bVilleoOk = false;
var bCPOk = false;

// Les RegEx :

var controleEspace = /  +/g;
var controleTiret = /--+/g;
var controleApostrophe = /''+/g;
var controleNom = /([^a-zA-Zâêîôûàèùòÿäëïöüÿéãñõ-\s])/; //Liste symboles acceptés pour Nom et Prénom
var controleMotDePasse = /([^a-zA-Z0-9âêîôûàèùòÿäëïöüÿéãñõ!-_])/;
var controleUtilisateur = /([^a-zA-Z0-9âêîôûàèùòÿäëïöüÿéãñõ])/;
var controleMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var controleAdresse = /([^a-zA-Z0-9âêîôûàèùòÿäëïöüÿéãñõ'-\s])/;
var controleVille = /([^a-zA-Zâêîôûàèùòÿäëïöüÿéãñõ'-\s])/
var controleCP = /([^0-9])/;

//Les abonnements :

monNom.addEventListener("keyup", verifNom);
monPrenom.addEventListener("keyup", verifPrenom);
monUtilisateur.addEventListener("keyup", verifUtilisateur);
monMotdepasse.addEventListener("keyup", verifMotdepasse);
monMail.addEventListener("keyup", verifMail);
monAdresse.addEventListener("keyup", verifAdresse);
maVille.addEventListener("keyup",verifVille);
monCP.addEventListener("keyup",verifCP);
btnRegister.addEventListener("click",verifChamp);


//Les fonctions :

//Fonctions vérifiants en temps réel si la saisie est correct :

function verifNom() {

    if (monNom.value.length == 0 || controleNom.test(monNom.value) || monNom.value.charAt(0) == " " || monNom.value.charAt(0) == "-"
        || monNom.value.charAt(monNom.value.length - 1) == " " || monNom.value.charAt(monNom.value.length - 1) == "-") {
        bNomOk = false;
    }
    else if (controleEspace.test(monNom.value)) {
        bNomOk = false;
    }
    else if (controleTiret.test(monNom.value)) {
        bNomOk = false;
    }
    else {
        //Nouveau contrôle espace et tiret sinon possibilité d'erreur :
        if (controleTiret.test(monNom.value)) {
            bNomOk = false;
        }
        else if (controleEspace.test(monNom.value)) {
            bNomOk = false;
        }
        else {
            bNomOk = true;
        }
    }

    if (bNomOk) {
        monNom.setAttribute("class", "champFormulaireOk");
    }
    else {
        monNom.setAttribute("class", "champFormulaire");
    }
}

function verifPrenom() {
    if (monPrenom.value.length == 0 || controleNom.test(monPrenom.value) || monPrenom.value.charAt(0) == " " || monPrenom.value.charAt(0) == "-"
        || monPrenom.value.charAt(monPrenom.value.length - 1) == " " || monPrenom.value.charAt(monPrenom.value.length - 1) == "-") {
        bPrenomOk = false;
    }
    else if (controleEspace.test(monPrenom.value)) {
        bPrenomOk = false;
    }
    else if (controleTiret.test(monPrenom.value)) {
        bPrenomOk = false;
    }
    else {
        //Nouveau contrôle espace et tiret sinon possibilité d'erreur :
        if (controleTiret.test(monPrenom.value)) {
            bPrenomOk = false;
        }
        else if (controleEspace.test(monPrenom.value)) {
            bPrenomOk = false;
        }
        else {
            bPrenomOk = true;
        }
    }

    if (bPrenomOk) {
        monPrenom.setAttribute("class", "champFormulaireOk");
    }
    else {
        monPrenom.setAttribute("class", "champFormulaire");
    }
}

function verifUtilisateur() {
    if (monUtilisateur.value.length < 3 || controleUtilisateur.test(monUtilisateur.value)) {
        bNomOk = false;
        monUtilisateur.setAttribute("class", "champFormulaire");
    }
    else {
        bUtilisateurOk = true;
        monUtilisateur.setAttribute("class", "champFormulaireOk");
    }
}

function verifMail() {
    if (controleMail.test(monMail.value)) {
        bMailOk = true;
        monMail.setAttribute("class", "champFormulaireOk");
    }
    else {
        bMailOk = false;
        monMail.setAttribute("class", "champFormulaire");
    }
}

function verifMotdepasse() {
    if (monMotdepasse.value.length < 5 || controleMotDePasse.test(monMotdepasse.value) || monMotdepasse.value.length > 12) {
        bMotdepasseOk = false;
        monMotdepasse.setAttribute("class", "champFormulaire");
    }
    else {
        bMotdepasseOk = true;
        monMotdepasse.setAttribute("class", "champFormulaireOk");
    }
}

function verifAdresse() {
    if (monAdresse.value.length < 3 || controleAdresse.test(monAdresse.value) || controleApostrophe.test(monAdresse.value) ||
        controleTiret.test(monAdresse.value) || controleEspace.test(monAdresse.value)) {
        bAdresseOk = false;
        monAdresse.setAttribute("class", "champFormulaire");
    }
    else {
        if (controleApostrophe.test(monAdresse.value) || controleTiret.test(monAdresse.value) || controleEspace.test(monAdresse.value)) {
            bAdresseOk = false;
            monAdresse.setAttribute("class", "champFormulaire");
        }
        else {
            bAdresseOk = true;
            monAdresse.setAttribute("class", "champFormulaireOk");
        }
    }
}

function verifVille(){
    if (maVille.value.length < 3 || controleVille.test(maVille.value) || controleApostrophe.test(maVille.value) ||
    controleTiret.test(maVille.value) || controleEspace.test(maVille.value)) {
    bAdresseOk = false;
    maVille.setAttribute("class", "champFormulaire");
}
else {
    if (controleApostrophe.test(maVille.value) || controleTiret.test(maVille.value) || controleEspace.test(maVille.value)) {
        bVilleoOk = false;
        maVille.setAttribute("class", "champFormulaire");
    }
    else {
        bVilleoOk = true;
        maVille.setAttribute("class", "champFormulaireOk");
    }
}
}

function verifCP(){
    if(monCP.value.length > 5 || controleCP.test(monCP.value) ||monCP.value.length < 5 ){
        bCPOk =false;
        monCP.setAttribute("class", "champFormulaire");
    }
    else{
        bCPOk=true;
        monCP.setAttribute("class", "champFormulaireOk");
    }
}

//Vérifie que tout les champs soient correctement remplis :
function verifChamp(){
    console.log("nom " + bNomOk)
    console.log("prenom " +bPrenomOk)
    console.log("utilisateur "+bUtilisateurOk)
    console.log("mdp "+bMotdepasseOk)
    console.log("Mail "+bMailOk)
    console.log("adresse "+bAdresseOk)
    console.log("Ville "+bVilleoOk)
    console.log("CP "+bCPOk)
    if(bNomOk && bPrenomOk && bUtilisateurOk && bMotdepasseOk && bMailOk && bAdresseOk && bVilleoOk && bCPOk){
        verifBaseDonneeClient();
    }
    else{
        alert("ERREUR CHAMP !")
    }
}

//Vérifie si doublon dans la base de donnée :

function verifBaseDonneeClient(){
    var i = 1; // indice
    var bInscriptionOk = true;
    do{
        var identifiantTemp = identifiants.get(i);
        if(monUtilisateur.value == identifiantTemp.nomUtilisateur){
            bInscriptionOk=false;
            alert("Nom d'utilisateur déjà pris");
            break;
        }
        else if(monMail.value == identifiantTemp.mail){
            bInscriptionOk=false;
            alert("Adresse mail déjà connue, un seul compte par adresse mail !")
        }
        else{
            bInscriptionOk=true;
        }
        
        i++;
    }while(i<=identifiants.size)
    if(bInscriptionOk){
        enregistrerClient()

    }
}


//Enregistre dans la base de donnée :

function enregistrerClient(){
    console.log(identifiants); // Avant enregistrement

    var nouvelIdentifiant ={
        nom:monNom.value,
        prenom:monPrenom.value,
        nomUtilisateur:monUtilisateur.value,
        compte:"client",
        adresse:monAdresse.value,
        ville:maVille.value,
        CP:monCP.value,
        mail:monMail.value,
        motDePasse:monMotdepasse.value,
        id:identifiants.size+1
    }
    identifiants.set((identifiants.size+1),nouvelIdentifiant);
    console.log(identifiants);//Après enregistrement
    alert("Inscription validée, bienvenu chez nous "+monUtilisateur.value);
}