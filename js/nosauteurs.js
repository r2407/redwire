// Les variables
var nbPages = 0;
var pageActuelle = 1;
var albparpage = 20;
var nbAlbumtotal = 0;
var auteurParPage = 20;
var nbAuteurTotal = 0;

nbAuteurTotal = parseInt(auteurs.size);
nbPages = Math.ceil(nbAuteurTotal / auteurParPage);

coverFlow(pageActuelle, nbAuteurTotal, auteurParPage, nbPages);



function coverFlow(pageActuelle, nbAlbumtotal, albparpage, nbPages) {
    var i = 1;
    var j = 0;
    do {
        var iString = i.toFixed(0);

        if (j < ((albparpage * pageActuelle) - albparpage)) {

            j++;
        }
        else if (auteurs.get(iString) && j < (nbAlbumtotal)) {
            var monAuteur = auteurs.get(iString);
            catSerie(monAuteur, i);
            j++;
        }
        else if (j == nbAlbumtotal) {
            break;
        }
        i++;
    } while (j < (albparpage * pageActuelle))
    navigate(pageActuelle, nbPages);
}

function catSerie(monAuteur, indice) {
    var nomAuteur = monAuteur.nom;


    var block = document.getElementById('afficheSerie');
    var div = document.createElement("div");
    div.setAttribute('class', 'speech');

    var span = document.createElement('span');
    span.innerHTML = "<h3>" + nomAuteur + "</h3>";
    div.addEventListener("click", function () { afficherCategorie(indice) })
    div.appendChild(span);
    block.appendChild(div);
}

function navigate(pageActuelle, nbPages) {
    var divBarre = document.getElementById("barre");
    divBarre.innerHTML = '';
    var symbBtnDebut = " &laquo;"   //double chevron gauche
    var symbBtnFin = " &raquo;"     //double chevron droite
    var symbBtnRecule = " &#10094;"    //chevron gauche
    var symbBtnAvance = " &#10095;"    //chevron droite
    var symbNumerique = "";
    //Bouton pour revenir premiere page :
    let btnDeb = 1;
    creatBouton(divBarre, btnDeb, symbBtnDebut);
    //Choix des boutons à afficher
    if (pageActuelle < 4) {
        //Boutons numériques :
        for (let k = 1; k <= 5; k++) {
            creatBouton(divBarre, k, symbNumerique);
        }
        //Bouton avance de 5 pages :
        let btnAv = pageActuelle + 5;
        if (btnAv > nbPages) {
            btnAv = nbPages;
        }
        creatBouton(divBarre, btnAv, symbBtnAvance);
    }
    else if (pageActuelle > 3 && pageActuelle < (nbPages - 3)) {
        //Bouton recule de 5 pages :
        let btnRec = pageActuelle - 5;
        if (btnRec <= 0) {
            btnRec = 1;
        }
        creatBouton(divBarre, btnRec, symbBtnRecule);
        //Boutons numériques :
        for (let k = pageActuelle - 2; k <= pageActuelle + 2; k++) {
            creatBouton(divBarre, k, symbNumerique);
        }
        //Bouton avance de 5 pages :
        let btnAv = pageActuelle + 5;
        if (btnAv > nbPages) {
            btnAv = nbPages;
        }
        creatBouton(divBarre, btnAv, symbBtnAvance);
    }
    else {
        let btnRec = pageActuelle - 5;
        if (btnRec <= 0) {
            btnRec = 1;
        }
        creatBouton(divBarre, btnRec, symbBtnRecule);
        for (let k = (nbPages - 5); k <= nbPages; k++) {
            if (k == 0) { k = 1 };
            creatBouton(divBarre, k, symbNumerique);
        }
    }
    //Boutton aller page 27 :
    let bEnd = nbPages;
    creatBouton(divBarre, bEnd, symbBtnFin);
}

function nouvellePage(k) {
    if (pageActuelle != k) {
        pageActuelle = k;
        let divCover = document.getElementById('afficheSerie');
        divCover.innerHTML = '';
        coverFlow(pageActuelle, nbAuteurTotal, auteurParPage, nbPages);
    }

}

function creatBouton(divBarre, k, symb) {
    let btn = document.createElement('a');
    btn.addEventListener("click", function () { nouvellePage(k) });
    btn.setAttribute('href', '#');
    btn.setAttribute("class", "w3-button");

    if (symb == "") {
        let symbAjout = " " + k;
        btn.innerHTML = symbAjout;
    }
    else {
        btn.innerHTML = symb
    }
    divBarre.appendChild(btn);
}

function afficherCategorie(indice) {

    var i = 1;
    indice = indice.toFixed(0);
    do {
        var iString = i.toFixed(0);
        if (albums.get(iString)) {
            if (albums.get(iString).idAuteur == indice) {
               
             window.location.href = "MonAuteur.html?Auteur="+indice
            }

        }
        i++;
    } while (i < 629)
}