function afficherPanierAvantPaiment() {
    shopCart = JSON.parse(sessionStorage.getItem('shopCart'));
    var cart = document.getElementById("recap");
    cart.onchange = function () { affichageDuPrixAPayer(); };
    cart.innerHTML = "";
    for (let i = 0; i < shopCart.length; i++) {
        var image = (shopCart[ i ][ 'image' ]);
        var prx = (shopCart[ i ][ 'prix' ]);
        var album = (shopCart[ i ][ 'nom' ]);
        var idAlbum = (shopCart[ i ][ 'id' ]);
        var btnPaiment = document.getElementById("topaiment");
        btnPaiment.addEventListener("click", function () { location.href = "paiment.html" });
        prx = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(parseFloat(prx / 100));

        var div = document.createElement("div");
        div.setAttribute("class", "ajoutitem dispositionPaiments");
        var img = document.createElement("img");
        img.setAttribute("src", image);
        img.setAttribute("class", "imgCart");
        var info = document.createElement("div");
        info.setAttribute("class", "speech");
        info.innerHTML = album + "<br>" + prx + "<br>";

        btnSupp = document.createElement("INPUT");
        info.appendChild(btnSupp);
        div.appendChild(img);
        div.appendChild(info);
        cart.appendChild(div);

        div.setAttribute("id", "div" + idAlbum);
        btnSupp.setAttribute("type", "button");
        btnSupp.setAttribute("value", "Supprimer");
        btnSupp.setAttribute("id", idAlbum)
        btnSupp.addEventListener("click", function () { actualisationPrix(shopCart, this) });
    }
    affichageDuPrixAPayer();
}
afficherPanierAvantPaiment();

function affichageDuPrixAPayer() {
    sum = total();

    var affichageDuPaiment = document.getElementById("recapAvantPaiment");
    var btnPayer = document.createElement("span");
    btnPayer.setAttribute('id', "btnpayer");
    btnPayer.innerHTML = "Commander"
    // btnPayer.addEventListener("click", function () { alert("on attend la page de connexion") });
    btnPayer.addEventListener("click",function(){validerCommande( new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(parseFloat(sum / 100)))});
    var totalAPayer = document.createElement("span");
    totalAPayer.setAttribute("id", "totalAPayer");
    totalAPayer.innerHTML = "Total : " + new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(parseFloat(sum / 100));
    affichageDuPaiment.appendChild(totalAPayer);
    affichageDuPaiment.appendChild(btnPayer);

}
function actualisationPrix(shopCart, element) {

    var idAsupp = element.id;
    var suppDiv = document.getElementById("div" + idAsupp);
    suppDiv.remove();

    for (let i = 0; i < shopCart.length; i++) {
        if (shopCart[ i ][ 'id' ] == idAsupp.toString()) {

            shopCart.splice(i, 1);
            sessionStorage.setItem('shopCart', JSON.stringify(shopCart));

        }
        var affichageDuPaiment = document.getElementById("recapAvantPaiment");
        affichageDuPaiment.innerHTML = "";
        affichageDuPrixAPayer();


    }

}

//Vérifie si l'utilisateur est connecté :
function validerCommande(prix){
    var idConnexion = sessionStorage.getItem("Idconnexion");
    if(!idConnexion || idConnexion==0){
        alert("Vous n'êtes pas connecté !")
    }
    else{

        alert("Vous avez commandé pour "+prix+" .Cela vous sera adressé dans les plus bref délais à cette adresse : "
        + identifiants.get(parseInt(idConnexion)).adresse+" "+identifiants.get(parseInt(idConnexion)).ville +" "+
        identifiants.get(parseInt(idConnexion)).CP);
    }

}